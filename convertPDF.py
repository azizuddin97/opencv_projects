import xlsxwriter
import base64
import pandas as pd
import pdfkit

class ConvertPDF:
    def __init__(self):
        pass

    def get_report(self, records):
        workbook = xlsxwriter.Workbook('downloads/employee_excel_page.xlsx')
        worksheet = workbook.add_worksheet()

        worksheet.write('A1', 'Attendance ID')
        worksheet.write('B1', 'Employee ID')
        worksheet.write('C1', 'Employee Surname')
        worksheet.write('D1', 'Attendance Date')
        worksheet.write('E1', 'Attendance Time')

        row = 1
        i = 0
        # Iterate over the data and write it out row by row.
        for record in records:
            column = 0
            worksheet.write(row, column, record[0])
            worksheet.write(row, column + 1, record[1])
            worksheet.write(row, column + 2, record[3])
            worksheet.write(row, column + 3, str(record[4]))
            worksheet.write(row, column + 4, str(record[5]))

            row += 1
            i =+ 1
        workbook.close()

        self.__turn_to_pdf('downloads/employee_excel_page.xlsx')


    def __turn_to_pdf(self, excel_page):
        df = pd.read_excel(excel_page)
        df.to_html("downloads/employee_html_page.html")
        pdfkit.from_file("downloads/employee_html_page.html", "downloads/employee_pdf_page.pdf")
