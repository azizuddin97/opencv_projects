## This class's objective is to connect to datbase,
## extract, delete, update and manipulate employees and attendance information

from mysql.connector import (connection) #to connect to database
from base64 import b64encode #to decode image from dadabase
from datetime import date #to obtain date

class Model:
    #constructor
    def __init__(self):
        #intialise connection with database
        self.cnx = connection.MySQLConnection(user='root', password='root',
                                         host='localhost',
                                         database='Dynamicvision')

    #method which inserts employee data to database
    #
    #parameters:
    #
    #request= array of employee info such as name, surname, position, DOB, photo etc.
    def insert_employee(self, request):
        #extract request data from form, using private method self.__extract_request(request)
        myDic = self.__extract_request(request)
        #log to database
        connect = self.cnx.cursor()
        #insert employee data
        connect.execute('INSERT INTO Employees (photo_id, name, surname, position, email, DOB, nationality, gender) VALUES (%s, %s, %s, %s, %s,  %s, %s, %s)',
        (myDic['image'].read(), myDic['name'].capitalize(), myDic['surname'].capitalize(), myDic['position'], myDic['email'], myDic['DOB'], myDic['nationality'].capitalize(), myDic['gender']))
        #commit insertion of info
        self.cnx.commit()
        #log out from database
        connect.close()

    #method which extracts employee info from HTML POST request form
    #
    #parameters:
    #
    #request= array of employee info such as name, surname, position, DOB, photo etc.
    def __extract_request(self, request):
        #dictionary where employee data is stored after extraction
        myDic = {}
        #clear dictionary
        myDic.clear()
        #insert employee data to dictionary
        myDic['name'] = request.form['Name']
        myDic['surname'] = request.form['Surname']
        myDic['position'] = request.form['Position']
        myDic['DOB'] = request.form['DOB']
        myDic['email'] = request.form['Email']
        myDic['nationality'] = request.form['Nationality']
        myDic['gender'] = request.form['Gender']
        #if immage available in form
        if request.files:
            myDic['image'] = request.files["Image"]
        return myDic

    #method whitch fetches data from database
    #
    #parameters:
    #
    #table= name of table from where to fetch
    def get_data(self, table):
        connect = self.cnx.cursor()
        connect.execute('SELECT * FROM %s' % table)
        records = connect.fetchall()
        connect.close()
        return records

    #method witch fetches images from a database and encode them
    #
    #parameters:
    #
    #table= name of table from where to fetch
    def get_images(self, table):
        #array where to store images
        images = []
        connect = self.cnx.cursor()
        #query
        connect.execute('SELECT photo_id FROM %s' % table )
        records = connect.fetchall()
        connect.close()
        #decode photo_id to be used in html pages
        for photo in records:
            images.append(b64encode(photo[0]).decode("utf-8"))
        return images

    #this method fetches employee data from database given their id (employees_id)
    #
    #parameters:
    #
    #employees_id = employee unique ID
    def get_employee_by_id(self, employees_id):
        #log to database
        connect = self.cnx.cursor()
        #execute query
        connect.execute('SELECT * FROM Employees WHERE employees_id = %s', (employees_id, ))
        #fetch all info
        records = connect.fetchall()
        #log out database
        connect.close()
        #return tuple of information
        return records

    #this method adds employee attendance information on Ateendance table
    #
    #parameters:
    #
    #employee_attendance = list of employee attendance obtained from camera detection
    #
    #employee_attendance_time = list of employee attendance time obtained from camera detection
    def add_record_attendance(self, employee_attendance, employee_attendance_time):
        today = date.today()
        connect = self.cnx.cursor()
        for attendance in employee_attendance:
            #check if employee id is already present in Attendance table
            connect.execute('SELECT * FROM Attendance WHERE employees_id = %s AND d_ate = %s', (attendance, today, ))
            myresult = connect.fetchone()
            #if result empty
            if not myresult:
                #obtain employee info from Employees table through employee id
                connect.execute('SELECT * FROM Employees WHERE employees_id = %s', (attendance, ))
                employee = connect.fetchone()
                #insert employee attendance to Attendance table
                connect.execute('INSERT INTO Attendance (employees_id, photo_id, surname, d_ate, t_ime) VALUES (%s, %s, %s, %s, %s)', (employee[0], employee[1], employee[3], today, employee_attendance_time[attendance]))
                self.cnx.commit()
        connect.close()

    #this method fetches employee attendace from database
    def get_attendance(self):
        self.__delete_attendance_if_not_today_date()
        connect = self.cnx.cursor()
        connect.execute('SELECT * FROM Attendance')
        records = connect.fetchall()
        connect.close()
        return records

    #this private method delete all rows that have
    # d_ate != not equal to today date
    def __delete_attendance_if_not_today_date(self):
        today = date.today()
        connect = self.cnx.cursor()
        connect.execute('DELETE FROM Attendance WHERE d_ate != %s', (today, ))
        self.cnx.commit()
        connect.close()

    #this method deletes row of Attendance table when the given
    #attendance_id is matched
    def delete_attendance_by_id(self, attendance_id):
        connect = self.cnx.cursor()
        connect.execute('DELETE FROM Attendance WHERE attendance_id = %s', (attendance_id, ))
        self.cnx.commit()
        connect.close()

    #this method deletes row of Employees table when the given
    #employees_id is matched
    def delete_employee_by_id(self, employees_id):
        connect = self.cnx.cursor()
        connect.execute('DELETE FROM Employees WHERE employees_id = %s', (employees_id, ))
        self.cnx.commit()
        connect.close()

    #decode image of a given tuple row
    def decode_image(self, employee_record):
        image = []
        for record in employee_record:
            image.append(b64encode(record[1]).decode("utf-8"))
        return image


    #update Employee table
    #
    #parameters:
    #
    #employees_id = employee ID used to identify which row to update
    #
    #request = an array of employee info to be updated
    def update_employee_info(self, employees_id, request):
        #extract request
        myDic = self.__extract_request(request)
        #log to database
        connect = self.cnx.cursor()
        #if myDic has myDic['image']- means image needs to be updated along other information
        if myDic['image']:
            print('passing here with image')
            connect.execute('UPDATE Employees SET photo_id = %s, name = %s, surname= %s, position= %s, email= %s, DOB= %s, nationality= %s, gender= %s WHERE employees_id = %s',
            (myDic['image'].read(), myDic['name'].capitalize(), myDic['surname'].capitalize(), myDic['position'], myDic['email'], myDic['DOB'], myDic['nationality'].capitalize(), myDic['gender'], employees_id, ))
            self.cnx.commit()
            connect.close()
        else:
            print('passed with no image')
            connect.execute('UPDATE Employees SET name = %s, surname= %s, position= %s, email= %s, DOB= %s, nationality= %s, gender= %s WHERE employees_id = %s',
            (myDic['name'].capitalize(), myDic['surname'].capitalize(), myDic['position'], myDic['email'], myDic['DOB'], myDic['nationality'].capitalize(), myDic['gender'], employees_id, ))
            self.cnx.commit()
            connect.close()
