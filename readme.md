************************
<b>What is DynamicVision ?</b>
************************

The aims of this technology is to detect the attendance of an employee and to maintain a record of those collected data into a database system. The presence of employees can be determined by capturing their face on to a high definition monitor video streaming service. The technique used to recognise human face is Feature-based approach.

*******************
<b>Working Principles</b>
*******************
<ul>
	<li>Capture: camera is positioned at the entrance of the building to capture entering employees.</li>
	<li>Extraction: captured video is converted into frames per second easier detection and recognition of employees.</li>
	<li>Comparison: process of face detection where the given input image is searched with images available into database system.</li>
	<li>Matching: after detecting and processing face, employee attendance sheet is updated with name, date and other valuable information.</li>
	<li>The system can be implemented anywhere such as schools, colleges, universities, airports and more. Poor lighting condition may affect image quality which 	indirectly degrades system performance.</li>

</ul>

************
<b>Screen-cast</b>
************

<div class="video-fallback">
  <a href="static/videos/addEmployee.mp4">Add Employee</a>
</div>

<div class="video-fallback">
  <a href="static/videos/manageEmployees.mp4">Manage Employees</a>
</div>

<div class="video-fallback">
  <a href="static/videos/faceRecognition.mp4">Face Recognition</a>
</div>

<div class="video-fallback">
  <a href="static/videos/attendancePDF.mp4">Download PDF Sheet of Employee Attendance</a>
</div>

<div class="video-fallback">
  <a href="static/videos/finalVideo.mp4">Overall Video</a>
</div>


*******************
<b>Technologies</b>
*******************
<ul>
	<li>I. Web Framework: Flask</li>
	<li>II. Web Template: Jinja</li>
	<li>III. Client Side: HTML, CSS, and JavaScript</li>
	<li>IV. Server Side: Python and MySQL</li>
	<li>V. Modules/Libraries: OpenCV, face_recognition, xlsxwriter, base64 and pdfkit</li>
</ul>

************
<b>Database Information</b>
************

<img src="static/images/Database.png" alt="DynamicVision Database Info" width="800" height="500">

************
<b>Additional Information</b>
************

The project is intend to be for educational purpose.
