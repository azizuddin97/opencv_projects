#This class's objective is to detect employee faces and detection time
import urllib
import face_recognition #face recognition module
import cv2 #open cv2 used for camera
import numpy as np
import io #modeule to convert images from string to Byte
from datetime import datetime as dt # to obtain employee face detection time

class FaceRecognition:
    #constructor
    def __init__(self):
        #store encoded face of employees which then
        #are used to match faces
        self.known_face_encodings = []
        #store employee names (specifically surname)
        self.known_face_surnames = []
        #store employee ids
        self.employee_id = []
        #store employee ID when employee face is etected
        self.employee_attendance = []
        #store face detection time
        self.current_time = {}
        #store detected face location from camera
        self.face_locations = []
        #store encoded face from camera
        self.face_encodings = []
        #store names which are used to lable faces in camera
        self.face_names = []

        self.process_this_frame = True
        print(cv2.__version__)

    def camera(self):
        #get default camera (usually front camer)
        cam = cv2.VideoCapture(0)

        #if is not camera is available
        if not cam.isOpened():
            print('Camera not available')
        else:
            #clear attendance from previous frames
            self.employee_attendance.clear()
            self.current_time.clear()
            while True:
                # Grab a single frame of video
                ret, frame = cam.read()
                # Resize frame of video to 1/4 size for faster face recognition processing
                small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

                # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
                rgb_small_frame = small_frame[:, :, ::-1]

                # Only process every other frame of video to save time
                if self.process_this_frame:
                    # Find all the faces and face encodings in the current frame of video
                    self.face_locations = face_recognition.face_locations(rgb_small_frame)
                    self.face_encodings = face_recognition.face_encodings(rgb_small_frame, self.face_locations)

                    self.face_names = []
                    for face_encoding in self.face_encodings:
                        # See if the face is a match for the known face(s)
                        matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
                        name = "Unknown"

                        # If a match was found in known_face_encodings, just use the first one.
                        if True in matches:
                            first_match_index = matches.index(True)
                            name = self.known_face_surnames[first_match_index]

                        # Or instead, use the known face with the smallest distance to the new face
                        face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
                        if len(self.known_face_encodings) != 0:
                            best_match_index = np.argmin(face_distances)
                            if matches[best_match_index]:
                                name = " ID:" + str(self.employee_id[first_match_index]) + " " + self.known_face_surnames[best_match_index]

                                #store employee ID when detected in employee_attendance list, store ID only once.
                                if not self.employee_id[first_match_index] in self.employee_attendance:
                                    self.employee_attendance.append(self.employee_id[first_match_index])
                                    #store detected time as well
                                    if self.employee_id[first_match_index] not in self.current_time.keys():
                                        self.current_time[self.employee_id[first_match_index]] = dt.now().time()

                        self.face_names.append(name)

                self.process_this_frame = not self.process_this_frame


                # Display the results
                for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
                    # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                    top *= 4
                    right *= 4
                    bottom *= 4
                    left *= 4

                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

                    # Draw a label with a name below the face
                    cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_DUPLEX
                    cv2.putText(frame, name, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)

                # Display the resulting image
                cv2.imshow('Facial Recognition Attendance', frame)

                # Hit 'esc' on the keyboard to quit!
                if cv2.waitKey(1) & 0xFF == 27 or cv2.getWindowProperty('Facial Recognition Attendance',1) == -1:
                    break

        # Release handle to the webcam
        cam.release()
        cv2.destroyAllWindows()


    #this method extract employee data (photo_id, surname, and employee ID) from a tuple
    def set__known_face_encodings(self, records):
        if(len(records) != 0):
            #for each row of tuple
            for record in records:
                #load image file to face_recognition
                #
                #io.BytesIO(record[1] = converts from string to Byte
                face = face_recognition.load_image_file(io.BytesIO(record[1]))
                #encode face
                face_encoding = face_recognition.face_encodings(face)[0]
                #append encoded face to known_face_encodings list
                self.known_face_encodings.append(face_encoding)
                #append employee surnames to known_face_surnames list
                self.known_face_surnames.append(record[3])
                #append employee ID to employee_id list
                self.employee_id.append(record[0])

    #this method return employee ID which where detected in camera
    def get_face_id(self):
        return self.employee_attendance

    #this method return employee face detection time
    def get_employee_attendance_time(self):
        return self.current_time
