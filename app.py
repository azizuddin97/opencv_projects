from flask import Flask, render_template, url_for, request, redirect, send_file, flash
from flaskwebgui import FlaskUI #get the FlaskUI class
#import Model class from model.py
from model import Model
#import FaceRecognition class from faceRecognition.py
from faceRecognition import FaceRecognition
#import ConvertPDF class from convertPDF.py
from convertPDF import ConvertPDF


app = Flask(__name__)
ui = FlaskUI(app)
#initialise model calss
model = Model()


#home page route
@app.route('/')
#method which deals with home page (index.html)
def index():
    #loads index.html from templetes/index.html
    return render_template('index.html')

#/add-employees/' page route
@app.route('/add-employees/', methods=['POST', 'GET'])
#method which deals with add employee into database
def add_employee():
    #when POST
    if request.method == 'POST':
        #insert requets into database
        model.insert_employee(request)
        #redirect to function add_employee()
        return redirect(url_for('add_employee'))
    #when GET
    elif request.method == 'GET':
        #load page templetes/addEmployee.html
        #
        #records = employees info such as id, name, surname, position etc.
        #images = employees photo id decoded and ready to be used in templetes/addEmployee.html page
        return render_template('addEmployee.html', records=model.get_data('Employees'), images=model.get_images('Employees'))

#/register-attendace/ page route
@app.route('/register-attendace/', methods=['POST', 'GET'])
#this method is resposible for registering employee attendance
#and to produce a PDF of attendaces of employees

def register_attendance():
    #initilise face_recognition, this deals recognizing employee faces
    face_recognition = FaceRecognition()
    #initialse extractPDF, this makes a pdf sheet of employee attendance
    extractPDF = ConvertPDF()
    if request.method == 'POST':
        if 'check_attendance' in request.form:
            #set employee images so they can be recognised
            face_recognition.set__known_face_encodings(model.get_data('Employees'))
            #open camera
            face_recognition.camera()
            print('passed here!!!')
            #store attendace to database
            model.add_record_attendance(face_recognition.get_face_id(), face_recognition.get_employee_attendance_time())
            return redirect(url_for('register_attendance'))
        elif 'download_pdf' in request.form:
            #make pdf file of attendances
            extractPDF.get_report(records=model.get_attendance())
            #send file so user can have it
            return send_file('downloads/employee_pdf_page.pdf', attachment_filename='Attendance_report.pdf', as_attachment=True)
    #when GET
    else:
        #render templetes/registerAttendance.html and update attendance table
        #with employee info and their respectice images
        return render_template('registerAttendance.html', records=model.get_attendance(), images=model.get_images('Attendance'))


#/delete-attendance/<attendance_id> page route
#<attendance_id> get attendance_id as prolog from registerAttendance.html
@app.route('/delete-attendance/<attendance_id>', methods=['POST', 'GET'])

#this method is resposible of deleting selected attendance rows
#
#parameters:
#
#attendance_id = attendance id which will be deleted from database
def delete_attendance(attendance_id):
    #delete attendance
    model.delete_attendance_by_id(attendance_id)
    #redirect to register_attendance()
    return redirect(url_for('register_attendance'))

#/manage-employees/ page route
@app.route('/manage-employees/',methods=['POST', 'GET'])
def manage_employees():
    return render_template('manageEmployees.html',records=model.get_data('Employees'), images=model.get_images('Employees'))

#/delete-employee/<employees_id> page route
#<employees_id> get employees_id as prolog from registerAttendance.html
@app.route('/delete-employee/<employees_id>', methods=['POST', 'GET'])
#this method is responsible for deleting the selected row from templetes/manageEmployees.html
#
#parameters:
#
#employees_id:employee ID
def delete_employee(employees_id):
    #delete row
    model.delete_employee_by_id(employees_id)
    #redirect to manage_employees()
    return redirect(url_for('manage_employees'))

#/update-employee/<employee_id> page route
#<employees_id> get employees_id as prolog from registerAttendance.html
@app.route('/update-employee/<employee_id>', methods=['POST', 'GET'])
def get_employee_for_update(employee_id):
    if request.method == 'GET':
        #get employee data from database and show in templetes/updateEmployee.html
        records = model.get_employee_by_id(employee_id)
        return render_template('updateEmployee.html', records = records, image=model.decode_image(records))
    elif request.method == 'POST':
        if 'cancel_update' in request.form:
            return redirect(url_for('manage_employees'))
        elif 'update_employee_info' in request.form:
            #update employee data when button pressed
            model.update_employee_info(employee_id, request)
            return redirect(url_for('manage_employees'))


#tutorial page route
@app.route('/tutorial/')
#method which deals with tutorial page(tutorial.html)
def tutorial():
    #loads tutorial.html from templetes/tutorial.html
    return render_template('tutorial.html')


#more page route
@app.route('/more/')
#method which deals with more page (more.html)
def more():
    #loads more.html from templetes/more.html
    return render_template('more.html')


ui.run()
# if __name__ == '__main__':
#     app.run(debug=True)
